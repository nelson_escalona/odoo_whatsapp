from odoo import models, fields, api, _
import urllib.parse as parse
from odoo.exceptions import UserError
from itertools import groupby

class SaleOrderValidation(models.Model):
    _inherit = 'sale.order'

    def parseNroOrder(self, nro):
        check = True
        result = nro
        idx = 0
        while check and idx < len(nro):
            check = nro[idx] == 'S' or nro[idx] == '0'
            if not check:
                result = nro[0:idx] + '-' + nro[idx:]
            idx = idx + 1
        return result

    def sale_whatsapp(self):
        record_phone = self.partner_id.mobile
        if not record_phone:
            view = self.env.ref('odoo_whatsapp.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "Please add a mobile number!"
            return {
                'name': 'Mobile Number Field Empty',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        if not record_phone[0] == "+":
            view = self.env.ref('odoo_whatsapp.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "No Country Code! Please add a valid mobile number along with country code!"
            return {
                'name': 'Invalid Mobile Number',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        else:
            return {'type': 'ir.actions.act_window',
                    'name': _('Whatsapp Message'),
                    'res_model': 'whatsapp.wizard',
                    'target': 'new',
                    'view_mode': 'form',
                    'view_type': 'form',
                    'context': {
                        'default_template_id': self.env.ref('odoo_whatsapp.whatsapp_sales_template').id},
                    }

    def send_direct_message(self):
        record_phone = self.partner_id.mobile
        if not record_phone:
            view = self.env.ref('odoo_whatsapp.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "Please add a mobile number!"
            return {
                'name': 'Mobile Number Field Empty',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        if not record_phone[0] == "+":
            view = self.env.ref('odoo_whatsapp.warn_message_wizard')
            view_id = view and view.id or False
            context = dict(self._context or {})
            context['message'] = "No Country Code! Please add a valid mobile number along with country code!"
            return {
                'name': 'Invalid Mobile Number',
                'type': 'ir.actions.act_window',
                'view_type': 'form',
                'view_mode': 'form',
                'res_model': 'display.error.message',
                'views': [(view.id, 'form')],
                'view_id': view.id,
                'target': 'new',
                'context': context
            }
        else:
            prods = ""
            for rec in self:
                for id in rec.order_line:
                    prods = prods + "*" +str(id.product_id.name) + " : " + str(id.product_uom_qty) + "* \n"

            custom_msg = "Hello *{}*, your Sale Order *{}* with amount *{} {}* is ready. \nYour order contains following items: \n{}".format(str(self.partner_id.name),str(self.name),str(self.currency_id.symbol),str(self.amount_total),prods)
            ph_no = [number for number in record_phone if number.isnumeric()]
            ph_no = "".join(ph_no)
            ph_no = "+" + ph_no

            link = "https://web.whatsapp.com/send?phone=" + ph_no
            message_string = parse.quote(custom_msg)

            url_id = link + "&text=" + message_string
            return {
                'type':'ir.actions.act_url',
                'url': url_id,
                'target':'new',
                'res_id': self.id,
            }

    def check_value(self, partner_ids):
        partners = groupby(partner_ids)
        return next(partners, True) and not next(partners, False)


    def multi_sms(self):
        sale_order_ids = self.env['sale.order'].browse(self.env.context.get('active_ids'))
        final_msg = ''




        for sale in sale_order_ids:
            order_subtotal = 0
            order_shipping = 0
            order_tax = 0
            order_total = 0


            current_order = self.parseNroOrder(sale.name)
            current_invoice = sale.invoice_name

            current_partner_phone = sale.partner_id.phone or ''
            current_partner_mobile = sale.partner_id.mobile or ''
            current_partner_user = sale.partner_id.name + "\nTeléfono: " + current_partner_phone + "\nCelular: " + current_partner_mobile + "\n"

            current_partner_street = sale.partner_id.street or ''
            current_partner_street2 = sale.partner_id.street2 or ''
            current_partner_state = sale.partner_id.state_id.name or ''
            current_partner_municipality = sale.partner_id.municipality_id.name or ''
            current_partner_country = sale.partner_id.country_id.name or ''
            current_partner_zip = sale.partner_id.zip or ''

            current_partner_address = "Dirección: \n" + current_partner_street + " " + current_partner_street2 + "\n" + current_partner_state + " " + current_partner_municipality + " " + current_partner_country + " " + current_partner_zip + "\n"



            current_phone = sale.partner_shipping_id.phone or ''
            current_mobile = sale.partner_shipping_id.mobile or ''
            current_shipping_user = sale.partner_shipping_id.name + "\nTeléfono: " + current_phone + "\nCelular: " + current_mobile + "\n"


            current_street = sale.partner_shipping_id.street or ''
            current_street2 = sale.partner_shipping_id.street2 or ''
            current_state = sale.partner_shipping_id.state_id.name or ''
            current_municipality = sale.partner_shipping_id.municipality_id.name or ''
            current_country = sale.partner_shipping_id.country_id.name or ''
            current_zip = sale.partner_shipping_id.zip or ''

            current_address = "Dirección: \n" + "Calle: " + current_street + " \n Pais: " + current_country + "\n Provincia: " + current_state + " \n Municipio: " + current_municipality + "\n Poblado o Barrio: " + current_street2 + " " + current_zip + "\n\n"

            prods = "La factura: " + "*" + current_invoice + "* contiene: \n"

            for id in sale.order_line:
                if 'Delivery' not in str(id.product_id.display_name):
                    prods = prods + "*" + "--" + str(id.product_id.display_name) + "*" + ", Qty: " + "*" + str(
                    id.product_uom_qty) + "*" + ", Unit Price: " + "*" + str("${:,.2f}".format(id.price_unit)) + "*" + ", Subtotal: " + "*" + str("${:,.2f}".format(id.price_subtotal)) + "*" + " \n"

                    order_subtotal = order_subtotal + id.price_subtotal
                    # order_shipping =
                    order_tax = order_tax + id.price_tax
                else:
                    order_shipping = id.price_subtotal

            order_total = order_subtotal + order_shipping + order_tax
            final_msg = final_msg + current_invoice + "\n\n" + "Facturado a: \n" + current_partner_user + current_partner_address + "\n" + "Entregar a: \n" + current_shipping_user + current_address + prods + "\n" + "Subtotal: " + str("${:,.2f}".format(order_subtotal)) + "\n" + "Shipping & Handling: " + str("${:,.2f}".format(order_shipping)) + "\n" + "Tax: " + str("${:,.2f}".format(order_tax)) + "\n" +  "Total: " + str("${:,.2f}".format(order_total)) + "\n\n\n"





        form_id = self.env.ref('odoo_whatsapp.whatsapp_multiple_message_wizard_form').id

        ctx = dict(self.env.context)
        ctx.update({
            'default_message': final_msg,
            'default_partner_id': self.partner_id[0].id,
            'default_mobile': self.partner_id[0].mobile,
        })
        return {
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'whatsapp.wizard.multiple.contact',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'target': 'new',
            'context': ctx,
        }

    # def multi_sms(self):
    #     sale_order_ids = self.env['sale.order'].browse(self.env.context.get('active_ids'))
    #
    #     cust_ids = []
    #     sale_nums = []
    #     for sale in sale_order_ids:
    #         cust_ids.append(sale.partner_id.id)
    #         sale_nums.append(sale.name)
    #
    #     # To check unique customers
    #     cust_check = self.check_value(cust_ids)
    #
    #     if cust_check:
    #         sale_numbers = sale_order_ids.mapped('name')
    #         sale_numbers = "\n".join(sale_numbers)
    #
    #         form_id = self.env.ref('odoo_whatsapp.whatsapp_multiple_message_wizard_form').id
    #         product_all = []
    #         for each in sale_order_ids:
    #             prods = ""
    #             for id in each.order_line:
    #                 prods = prods + "*" + "Product: "+str(id.product_id.name) + ", Qty: " + str(id.product_uom_qty) + "* \n"
    #             product_all.append(prods)
    #
    #         custom_msg = "Hi" + " " + self.partner_id.name + ',' + '\n' + "Your Sale Orders" + '\n' + sale_numbers + \
    #                      ' ' + '\n' + "are ready for review.\n"
    #         counter = 0
    #         for every in product_all:
    #             custom_msg = custom_msg + "Your order " + "*" + sale_nums[
    #                 counter] + "*" + " contains following items: \n{}".format(every) + '\n'
    #             counter += 1
    #
    #         final_msg = custom_msg + "\nDo not hesitate to contact us if you have any questions."
    #
    #         ctx = dict(self.env.context)
    #         ctx.update({
    #             'default_message': final_msg,
    #             'default_partner_id': self.partner_id.id,
    #             'default_mobile': self.partner_id.mobile,
    #         })
    #         return {
    #             'type': 'ir.actions.act_window',
    #             'view_mode': 'form',
    #             'res_model': 'whatsapp.wizard.multiple.contact',
    #             'views': [(form_id, 'form')],
    #             'view_id': form_id,
    #             'target': 'new',
    #             'context': ctx,
    #         }
    #     else:
    #         raise UserError(_('Please Select Orders of Unique Customers'))